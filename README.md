# GS FED Boilerplate

## Node Dependency

Node v6.1.0 (npm v3.8.6) is required for this version of the GS FED Boilerplate to work.

OS X users can use `n` available through npm for Node version management.
Windows users can make use of `nodist`.

### Node Module Dependencies

We've had a hell of a time internally with running `npm install` on older projects, because of the shifting nature of the npm universe.

It's now a requirement for projects to include a tar.gz file of the node_modules folder. It's not an ideal solution but it is the least worst solution to getting a project up and running with minimal effort.

There are two script commands to handle this `npm run watch:node_modules` and `npm run zip:node_modules`. Watch keeps track of the package.json and triggers the second zip function. Using zip will tarball the node_modules directory and gzip it (works cross platform!). This file should then be committed to the repo.

Keeping tar.gz up to date is _extremly important_, espeically on mature projects.

## Tasks

### Running the Project

The project can be set up and run in watch mode with the following commands:

    npm install
    npm start

An auto-refresh server will now be available at [http://localhost:3000](http://localhost:3000).

### Building the Project

The project can be compiled out to the dist folder with the following commands:

    npm install
    npm run build

### Linting the Project

Linting should be done during development using your editor of choices `eslint` package.

As a backup the boilerplate will lint JS files at build time automatically.

### Cleaning the Project

The project can be cleaned with:

    npm run pre:build

This will remove the `dist` folder.

## Directory Structure

- `src` - The parent directory for all source files
    - `assets` - This directory will be copied directly into the `dist` folder. Should contain any media (fonts/images) that are referenced directly within the templates.
    - `js` - The parent directory for all javascript files. This is for any common or page specific javascript.
    - `components` - The parent directory for all components. See "Creating a Component" for more information.
    - `scss` - The parent directory for all stylesheets. This is for any common or page specific styles.
    - `templates` - The parent directory for all page templates.
        - `layouts` - Contains layout files. Master layout should be in here.
        - `pages` - Contains all page templates.
- `dist` - The output directory for builds run through `npm run build`.

## Branching strategy

* master - main production version, base for new projects (always production code, you shouldn't ever be committing to master)
* dev - development version for testing windows/mac/build server tests
    * other-development-branches
* experimental - major (but working) experimental version (synced with dev)
    * other-experimental-branches

## Cloning for new projects

New projects should be kept up to date with the original boilerplate repo.

1. Clone the boilerplate repo with the origin set to "boilerplate"
    * `git clone -o boilerplate git@bitbucket.org:Get-Started/gs-fed-boilerplate.git "new-project-name"`
    * `cd new-project-name`
2. Add a new origin for new projects' repo
    * `git remote add origin <new-repo-url-from-bitbucket>`
3. Fetch or pull from boilerplate master when updating.
    * `git fetch boilerplate master` / `git pull boilerplate master`

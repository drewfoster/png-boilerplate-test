var assemble = require('assemble');
var extname = require('gulp-extname');
var app = assemble();

app.task('load', function(cb) {
  app.partials('src/components/**/*.hbs');
  app.layouts('src/templates/layouts/*.hbs');
  app.pages('src/templates/pages/*.hbs');
  app.data({templateListing: getPageData()});
  cb();
});

app.task('default', ['load'], function() {
  return app.toStream('pages')
    .pipe(app.renderFile())
    .pipe(extname())
    .pipe(app.dest('dist'));
});

module.exports = app;

function getPageData() {
    var fs = require('fs');
    var pages = fs.readdirSync('src/templates/pages');
    return pages.filter(function(el) {
        return el !== 'index.hbs';
    }).map(function(el) {
        return {
            name: capitalize(el.replace('.hbs', '').replace(/-/g, ' ')),
            url: el.replace('.hbs', '.html')
        };
    });
}
function capitalize(input) {
    return input.split(' ').map(function(el) {
        return el.charAt(0).toUpperCase() + el.substring(1);
    }).join(' ');
}

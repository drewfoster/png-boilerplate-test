'use strict';

import $ from 'jquery';

function componentWithJsInit() {
   $('.component-with-js h2').text('This should say "hello world!""');
}

componentWithJsInit();

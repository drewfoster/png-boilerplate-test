# GS FED Boilerplate

** possible need for npm install -g tar.gz on windows if they want/need to do node_modules zipping, needs more testing though **

#todo#
add in
    header
    footer
    example modules from noooze
    add in css wipe
add in slick Stylesheet
    clean up Stylesheet
    add in more example js
write documentation for why kitchen sink, development processed
no bower installs, fucking hate bower installs
    add in docs about node modules

##bugs
styles.scss pumps out twice because of the watch change.

## Node Dependency

Node v6.1.0 (npm v3.8.6) is required for this version of the GS FED Boilerplate to work.

OS X users can use `n` available through npm for Node version management.
Windows users can make use of `nodist`.

### Node Module Dependencies

We've had a hell of a time internally with running `npm install` on older projects, because of the shifting nature of the npm universe.

It's now a requirement for projects to include a tar.gz file of the node_modules folder. It's not an ideal solution but it is the least worst solution to getting a project up and running with minimal effort.

There are two script commands to handle this `npm run watch:node_modules` and `npm run zip:node_modules`. Watch keeps track of the package.json and triggers the second zip function. Using zip will tarball the node_modules directory and gzip it (works cross platform!). This file should then be committed to the repo.

Keeping tar.gz up to date is _extremly important_, espeically on mature projects.

## Tasks

### Running the Project

The project can be set up and run in watch mode with the following commands:

    npm install
    npm start

An auto-refresh server will now be available at [http://localhost:3000](http://localhost:3000).

### Building the Project

The project can be compiled out to the dist folder with the following commands:

    npm install
    npm run build

### Linting the Project

Linting should be done during development using your editor of choices `eslint` package.

As a backup the boilerplate will lint JS files at build time automatically.

### Cleaning the Project

The project can be cleaned with:

    npm run pre:build

This will remove the `dist` folder.

## Directory Structure

- `src` - The parent directory for all source files
    - `assets` - This directory will be copied directly into the `dist` folder. Should contain any media (fonts/images) that are referenced directly within the templates.
    - `js` - The parent directory for all javascript files. This is for any common or page specific javascript.
    - `modules` - The parent directory for all modules. See "Creating a Module" for more information.
    - `scss` - The parent directory for all stylesheets. This is for any common or page specific styles.
    - `templates` - The parent directory for all page templates.
        - `layouts` - Contains layout files. Master layout should be in here.
        - `pages` - Contains all page templates.
- `dist` - The output directory for builds run through `npm run build`.

## Upgrading an existing project to latest framework
1. Open downstream project and switch to working branch
2. From 'origin' menu, select 'Pull upstream/master into [current working branch]'
3. Resolve conflicts in package.json (using upstream master dep versions)
    - There will likely be a bunch of conflicts in the .scss files.  You want to resolve all of these using 'Mine'.
    - Also, there will be conflicts with your layout.marko template.  You want to go through this manually and remove the boilerplate markup that has come through from upstream
4. run `rm -rf node_modules`
5. run `npm install`
7. npm start

## Creating a Template

Marko files can be added to the `flats` directory and will automatically be added to the build. A standard template should attempt to provide a layout and allow functionality of individual sections to be implemented by components. A typical template will use a master page with the following syntax:

    <layout-use template="../layout.marko">
        <layout-put into="body">
            <!-- Your content -->
        </layout-put>
    </layout-use>

This example assumes the master page has provided a placeholder titled "body" by including the following snippet:

    <layout-placeholder name="body"/>

Additional placeholders can be added to allow page templates to insert content in multiple places within the master page.

## Registering a Component

As the eventual export from the FED repo is CSS and JS, each component must be registered inside the `src/index.js`. The `component.js` file will be referenced for each item and will produce an index.js that looks similar to:

    // base styles
    import './styles/styles.scss';

    // components
    import './components/my-component/component';
    import './components/my-other-component/component';


## Creating a Component

Components are the backbone of JCVD and should be responsible for the majority of functionality and styling of the page. All components should be contained within a folder that _must_ have a hyphen within the name. The hyphen is required to allow the Marko templating language to distinguish between standard HTML components and custom components. If the component is created inside the `components` folder, it can be referenced as if it was a standard HTML tag.

    <my-component my-attribute="My Value"/>

Any additional component directories (e.g. a `banners` directory containing all banners for a site) can be added to the `marko-taglib.json` in the root of the project.

 A component may include the following files:

### template.marko

This is the template to be used for rendering the markup for an individual component. Marko syntax can be read about [in their documentation](http://markojs.com/docs/marko/language-guide/).

Templates should always have a wrapping element with the `data-component` attribute set to the name of the component. For example:

    <div data-component="my-component">
        <!-- Component markup -->
    </div>

### component.js

This is the main entry point for an individual component to allow it to be registered in the Webpack bundle. It should import any styles and functionality required by the module and invoke the `default` function of the UX code. A standard `component.js` will look similar to this:

    import './my-component.scss';
    import myComponent from './my-component.js';
    myComponent();

### [component-name].js

Functionality for an instance of a component should be defined here. Helpers have been provided to aid in the instantiation of components and to allow the exposed method to be testable. A typical component will look similar to:

    import { initComponent } from '../../js/component';

    // default export which calls initComponent with the selector and component function
    export default function(window) {
        initComponent(window, '[data-component="homepage-title"]', createComponent);
    }

    // the component function method will receive the DOM node and an instance of jQuery
    function createComponent(component, $) {
        $('h1', component).text('My Title');
    }

Any DOM interactions should be made against the component's DOM node which has been passed to the component function. The simplest way to achieve this is to pass the component's DOM node as the second argument to any jQuery selectors as seen above in `createComponent`.

### [component-name].scss

Styles for an instance of a component should be defined here. While nesting of individual rules is up to the discretion of the developer, it is expected that all styles will be wrapped in a selector matching the `data-component` attribute of the component. For example:

    [data-component="my-component"] {
        .my-class { ... }
        .my-other-class { ... }
    }
